# install necessary packegs

install.packages(c ("RWeka"), dependencies = TRUE )

# load 
library(RWeka)

# prepare the datasetlibraries
loc = "C:\\Users/gv813981/Documents/prism_in_r/Data/car.csv"

# read data as a csv file
carDataset <- read.table (loc, sep=",", header=F)   

# add header to the data
names (carDataset) <- c ( "buying", "maint", "doors", "persons", "lug_boot", "safety", "class" )

#  check number of attributes before excluding the class label = 7 (extra info)
totalAttr <- ncol (carDataset) 


# remove the class label from the dataset    (extra info)
carDataNoClassLabel <- carDataset [-7]    

#  check number of attributes after excluding the class label = 6  (extra info)
numAttrNoClass <- ncol ( carDataNoClassLabel)   

# get list of all possible class labels
classLabels <- c(unique(subset(carDataset, select = c("class"))))  # this step is unnecassary, 
# it can be replaced by:
# levels(carDataset$class)

# get the number of  classes in the datastet  (extra info) 
numOfClasses <- length(unique(carDataset$class))   # = 4     

# get the list of all possible class values
classesList <- levels(carDataset$class)

# loop through each class
#for ( i in classLabels$class ) { 


for (i in classesList) {
  
  # Create D based on original dataset
  datasetD <- carDataset
  
  while ( f.classesOtherThanGivenClass (datasetD, i) == TRUE ) {
    print ("Hi")
    
    #  loop through each attribute in DatasetD
    
    
  }   # end of while Loop
  
}   # end of for Loop

# Function to check if there are instances are not covered by a given class
f.classesOtherThanGivenClass <- function( D, inputClass) {
  
  # contains other than the input classs
  containsOtherClasses <- FALSE
  
  print (c("before loop", containsOtherClasses))
  
  # go through each instance in the datasetD
  for ( h in D$class ) { 
    
    if( h != inputClass){
      containsOtherClasses <- TRUE
      print(c("this class is",inputClass))
      print (containsOtherClasses)
      
      break()
    }
    
  }
  
  print (c( "after loop",containsOtherClasses))
  print (c("Dataset contains classes other than current class"))
  return (containsOtherClasses) }


##############################














rm (f.classesOtherThanGivenClass, classesList,classLabels,i, numOfClasses)
