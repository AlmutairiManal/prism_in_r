


# knn (train, test, cl, k = 3, prob=TRUE)
# knn.cv function source code

train <- rbind(iris3[1:25,,1], iris3[1:25,,2], iris3[1:25,,3])
test <- rbind(iris3[26:50,,1], iris3[26:50,,2], iris3[26:50,,3])
cl <- factor(c(rep("s",25), rep("c",25), rep("v",25)))
myKnn(train, test, cl, k = 3, prob=TRUE)
attributes(.Last.value)

#  function (train, cl, k = 1, l = 0, prob = FALSE, use.all = TRUE) 
# {
#   train <- as.matrix(train)
#   if (any(is.na(train)) || any(is.na(cl))) 
#     stop("no missing values are allowed")
#   p <- ncol(train)
#   ntr <- nrow(train)
#   if (length(cl) != ntr) 
#     stop("'train' and 'class' have different lengths")
#   if (ntr - 1 < k) {
#     warning(gettextf("k = %d exceeds number %d of patterns", 
#                      k, ntr - 1), domain = NA)
#     k <- ntr - 1
#   }
#   if (k < 1) 
#     stop(gettextf("k = %d must be at least 1", k), domain = NA)
#   clf <- as.factor(cl)
#   nc <- max(unclass(clf))
#   Z <- .C(VR_knn, as.integer(k), as.integer(l), as.integer(ntr), 
#           as.integer(ntr), as.integer(p), as.double(train), as.integer(unclass(clf)), 
#           as.double(train), res = integer(ntr), pr = double(ntr), 
#           integer(nc + 1), as.integer(nc), as.integer(TRUE), as.integer(use.all))
#   res <- factor(Z$res, levels = seq_along(levels(clf)), labels = levels(clf))
#   if (prob) 
#     attr(res, "prob") <- Z$pr
#   res
# }




# knn function source code

myKnn <- function (train, test, cl, k = 1, l = 0, prob = FALSE, use.all = TRUE) 
{
  train <- as.matrix(train)
  if (is.null(dim(test))) 
    dim(test) <- c(1, length(test))
  test <- as.matrix(test)
  if (any(is.na(train)) || any(is.na(test)) || any(is.na(cl))) 
    stop("no missing values are allowed")
  p <- ncol(train)
  ntr <- nrow(train)
  if (length(cl) != ntr) 
    stop("'train' and 'class' have different lengths")
  if (ntr < k) {
    warning(gettextf("k = %d exceeds number %d of patterns", 
                     k, ntr), domain = NA)
    k <- ntr
  }
  if (k < 1) 
    stop(gettextf("k = %d must be at least 1", k), domain = NA)
  nte <- nrow(test)
  if (ncol(test) != p) 
    stop("dims of 'test' and 'train' differ")
  clf <- as.factor(cl)
  nc <- max(unclass(clf))
  Z <- .C(VR_knn, as.integer(k), as.integer(l), as.integer(ntr), 
          as.integer(nte), as.integer(p), as.double(train), as.integer(unclass(clf)), 
          as.double(test), res = integer(nte), pr = double(nte), 
          integer(nc + 1), as.integer(nc), as.integer(FALSE), as.integer(use.all))
  res <- factor(Z$res, levels = seq_along(levels(clf)), labels = levels(clf))
  if (prob) 
    attr(res, "prob") <- Z$pr
  res
}
# <bytecode: 0x0000000018574cb0>
# <environment: namespace:class>




